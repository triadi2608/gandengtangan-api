<?php

if (!function_exists('resJson')) {
    function resJson($code, $message, $data) {
        return response()->json([
            'code'    => $code,
            'status'  => in_array($code, [200, 201, 204]) ? strtoupper(__('global.succeed')) : strtoupper(__('global.failed')),
            'lang'    => app('translator')->getLocale(),
            'message' => $message !== null ? $message : null,
            'data'    => $data !== null ? $data : null,
        ], $code);
    }
}

if (!function_exists('token')) {
    function token($userId, $deviceId) {
        return hash('sha256', $userId . '_' . $deviceId);
    }
}

if (!function_exists('createActivity')) {
    function createActivity($userId = null, $parameterId = null, $type, $status, $createdAt = null) {
        DB::table('activities')->insert([
            'user_id'      => $userId,
            'parameter_id' => $parameterId,
            'type'         => $type,
            'status'       => $status,
            'created_at'   => $createdAt ? : date('Y-m-d H:i:s'),
            'updated_at'   => date('Y-m-d H:i:s'),
        ]);
    }
}
