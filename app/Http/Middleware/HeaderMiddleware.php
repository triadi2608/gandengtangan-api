<?php

namespace App\Http\Middleware;

use Closure;

class HeaderMiddleware
{
    public function handle($request, Closure $next)
    {
        if ((!isset($_SERVER['HTTP_CONTENT_TYPE']) || $_SERVER['HTTP_CONTENT_TYPE'] !== 'application/json') && (!isset($_SERVER['CONTENT_TYPE']) || $_SERVER['CONTENT_TYPE'] !== 'application/json')) {
            return resJson(403, 'Unauthorized header such as Content-Type', null);
        }

        $language_path = resource_path() . '/lang/';
        $languages = glob($language_path . '*' , GLOB_ONLYDIR);
        $accept_language = [];

        foreach ($languages as $key => $value) {
            $accept_language[] = str_replace($language_path, '', $value);
        }

        if ((!isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) || in_array(strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']), $accept_language) !== true) && (!isset($_SERVER['ACCEPT_LANGUAGE']) || in_array(strtolower($_SERVER['ACCEPT_LANGUAGE']), $accept_language) !== true)) {
            return resJson(403, 'Unauthorized header such as Accept-Language', null);
        } else {
            app('translator')->setLocale(strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']));
        }

        if ((!isset($_SERVER['HTTP_HTTP_X_API_KEY']) || $_SERVER['HTTP_HTTP_X_API_KEY'] !== env('APP_KEY')) && (!isset($_SERVER['HTTP_X_API_KEY']) || $_SERVER['HTTP_X_API_KEY'] !== env('APP_KEY'))) {
            return resJson(403, 'Unauthorized header such as x-api-key', null);
        }

        return $next($request);
    }
}
