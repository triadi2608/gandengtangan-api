<?php

namespace App\Http\Controllers\Lender\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required|min:6',
            'device_id' => 'required',
        ]);

        if ($validator->fails()) {
            $message = [
                'validation' => $validator->errors(),
            ];

            return resJson(400, $message, null);
        }

        $lender = (
            DB::table('user')
                ->where('email', strtolower($request->input('email')))
                ->where('level_akses', 'lender')
                ->select(
                    'id',
                    'name',
                    'email',
                    'password')
                ->first()
        );

        if (!$lender) {
            $message = __('auth.login.not_registered');

            return resJson(404, $message, null);
        } else {
            $checkPassword = app('hash')->check($request->input('password'), $lender->password);

            if ($checkPassword !== true) {
                $message = __('auth.login.wrong_password');

                return resJson(401, $message, null);
            } else {
                $token = token($lender->id, $request->input('device_id'));

                $sessions = json_decode(app('redis')->get($token));

                if (!$sessions) {
                    $sessions = [
                        'lender' => [
                            'id'        => $lender->id,
                            'name'      => $lender->name,
                            'email'     => $lender->email,
                            'device_id' => $request->input('device_id'),
                        ],
                    ];

                    app('redis')->set($token, json_encode($sessions));
                }

                $message = __('auth.login.succeed');
                $data = [
                    'token' => $token,
                ];

                return resJson(200, $message, $data);
            }
        }
    }
}
