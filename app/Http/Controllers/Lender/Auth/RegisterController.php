<?php

namespace App\Http\Controllers\Lender\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Validator;

class RegisterController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'         => 'required',
            'email'        => 'required|email|unique:user',
            'mobile_phone' => 'required|max:16',
            'password'     => 'required|min:6',
            'pwd_conf'     => 'required|same:password',
            'tnc'          => 'required|accepted',
            'device_id'    => 'required',
        ]);

        if ($validator->fails()) {
            $message = [
                'validation' => $validator->errors(),
            ];

            return resJson(400, $message, null);
        }

        $lenderId = (
            DB::table('user')->insertGetId([
                'name'          => $request->input('name'),
                'email'         => strtolower($request->input('email')),
                'verifikasi'    => 1,
                'verifikasi_hp' => rand(0000,9997),
                'flag'          => 1,
                'forgot_token'  => str_random(20),
                'level_akses'   => 'lender',
                'password'      => app('hash')->make($request->input('password')),
                'ref'           => 'direct',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ])
        );

        DB::table('user_detail')->insert([
            'user_id'        => $lenderId,
            'telpon_pribadi' => $request->input('mobile_phone'),
            'agree_tnc'      => 1,
            'created_at'     => date('Y-m-d H:i:s'),
            'updated_at'     => date('Y-m-d H:i:s'),
        ]);

        DB::table('user_balance')->insert([
            'user_id'    => $lenderId,
            'debit'      => 0,
            'credit'     => 0,
            'balance'    => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        createActivity($lenderId, null, 'register', 'verifying');

        $sessions = [
            'lender' => [
                'id'           => $lenderId,
                'name'         => $request->input('name'),
                'email'        => $request->input('email'),
                'mobile_phone' => $request->input('mobile_phone'),
                'device_id'    => $request->input('device_id'),
                'balance'   => [
                    'debit'   => 0,
                    'credit'  => 0,
                    'balance' => 0,
                ],
            ],
        ];

        $token = token($lenderId, $sessions['lender']['device_id']);

        app('redis')->set($token, json_encode($sessions));

        $message = __('auth.register.succeed');
        $data = [
            'token' => $token,
        ];

        return resJson(200, $message, $data);
    }
}
