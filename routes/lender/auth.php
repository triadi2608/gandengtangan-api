<?php

$router->group(['namespace' => 'Auth'], function() use ($router) {

    $router->post('/login', 'LoginController@login');
    $router->post('/register', 'RegisterController@store');

});
