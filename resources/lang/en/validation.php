<?php

return [

    'accepted'      => 'The :attribute must be accepted.',
    'email'         => 'The :attribute must be a valid email address.',
    'max'           => [
        'string' => 'The :attribute may not be greater than :max characters.',
    ],
    'min'           => [
        'string' => 'The :attribute must be at least :min characters.',
    ],
    'required'      => 'The :attribute field is required.',
    'same'          => 'The :attribute and :other must match.',
    'unique'        => 'The :attribute has already been taken.',

    'attributes'    => [
        'name'         => 'Name',
        'email'        => 'Email',
        'mobile_phone' => 'Mobile Phone',
        'password'     => 'Password',
        'pwd_conf'     => 'Password Confirmation',
        'tnc'          => 'Terms and Conditions',
        'device_id'    => 'Device Id',
    ],

];
