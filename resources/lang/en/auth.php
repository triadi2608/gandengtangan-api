<?php

return [

    'login'    => [
        'not_registered' => 'Email address not registered.',
        'succeed'        => 'Successfully logged in.',
        'wrong_password' => 'Incorrect password.',
    ],
    'register' => [
        'succeed' => 'Successfully registered.',
    ]

];
