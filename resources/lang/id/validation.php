<?php

return [

    'accepted'      => ':attribute harus diterima.',
    'email'         => ':attribute harus berupa alamat email yang valid.',
    'max'           => [
        'string' => ':attribute tidak boleh lebih dari :max karakter.',
    ],
    'min'           => [
        'string' => ':attribute harus setidaknya :min karakter.',
    ],
    'required'      => 'Kolom :attribute wajib diisi.',
    'same'          => ':attribute dan :other harus cocok.',
    'unique'        => ':attribute sudah diambil.',

    'attributes'    => [
        'name'         => 'Nama',
        'email'        => 'Email',
        'mobile_phone' => 'Telepon Selular',
        'password'     => 'Kata Sandi',
        'pwd_conf'     => 'Konfirmasi Kata Sandi',
        'tnc'          => 'Syarat dan Ketentuan',
        'device_id'    => 'Id Perangkat',
    ],

];
