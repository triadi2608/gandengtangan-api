<?php

return [

    'login'    => [
        'not_registered' => 'Alamat email tidak terdaftar.',
        'succeed'        => 'Berhasil masuk.',
        'wrong_password' => 'Kata Sandi salah.',
    ],
    'register' => [
        'succeed' => 'Pendaftaran berhasil.',
    ]

];
